FROM composer:latest AS composer

WORKDIR /app/
COPY composer.* ./ 
RUN composer install

FROM php:7.4-cli

COPY . /app/deputy
COPY --from=composer /app/vendor /app/deputy/vendor

WORKDIR /app/deputy