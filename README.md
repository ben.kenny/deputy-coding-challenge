# Deputy Coding Challenge

Author: Ben Kenny - ben.kenny@gmail.com

## Usage
The solution comes packages ready for use with the included `Dockerfile`

### Building
```
$ docker build -t deputy-challenge .
```

### Running the tests
```
$ docker run --rm -it deputy-challenge:latest vendor/bin/phpunit tests
```

### Running the app
The app can also be run on it's own outside the test harness. The app has a number of command-line parameters to allow for ease of running.
```
app.php -i<USERID> [-u"USER FILE"] [-r"ROLE FILE"]
```

#### Example usage

```
# print all the subordinates for user 1
docker run --rm -it deputy-challenge:latest php app.php -i1

# print all the subordinates for user 3
docker run --rm -it deputy-challenge:latest php app.php -i3

# mount a dir on the host into the container, and use that data
docker run --rm -v /path/to/data:/app/deputy/moredata -it deputy-challenge:latest php app.php -i1 -u"moredata/users.json"
```

