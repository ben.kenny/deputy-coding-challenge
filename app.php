<?php
require_once('vendor/autoload.php');

use Deputy\CodingChallenge\Store\FileRoleStore;
use Deputy\CodingChallenge\Store\FileUserStore;
use Deputy\CodingChallenge\Service\RoleService;
use Deputy\CodingChallenge\Service\UserService;


$options = getopt('r::u::i:');

$rolePath = $options['r'] ?? 'data/roles.json';
$userPath = $options['u'] ?? 'data/users.json';
$userId = $options['i'] ?? 1;

$roleService = new RoleService(new FileRoleStore($rolePath));
$userService = new UserService(new FileUserStore($userPath), $roleService);


var_dump($userService->findSubordinates($userId));
