<?php
namespace Deputy\CodingChallenge\Model;

class Role 
{
    public int $id;
    public string $name;
    public int $parentId;

    public function hydrate(array $values)
    {
        $values = array_change_key_case($values);

        $this->id = $values['id'];
        $this->name = $values['name'];
        $this->parentId = $values['parent'];
    }

}