<?php
namespace Deputy\CodingChallenge\Model;

class User
{
    public int $id;
    public string $name;
    public int $roleId;

    public function hydrate(array $values)
    {
        $values = array_change_key_case($values);

        $this->id = $values['id'];
        $this->name = $values['name'];
        $this->roleId = $values['role'];
    }
}