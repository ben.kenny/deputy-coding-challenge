<?php
namespace Deputy\CodingChallenge\Service;

use Deputy\CodingChallenge\Model\Role;
use Deputy\CodingChallenge\Store\RoleStore;
use Deputy\CodingChallenge\Store\FileRoleStore;

class RoleService
{
    protected RoleStore $store;

    public function __construct(RoleStore $roleStore = null)
    {
        $this->store = $roleStore ?? new FileRoleStore();
    }

    public function findAll() : array
    {
        return $this->store->getAll();
    }

    public function find(int $roleId) : Role
    {
        return $this->store->get($roleId);
    }

    public function findAllChildren(int $parentId) : array
    {
        $children = [];
        //first get all the direct decendants of the role, ie: they have parentId = roleId
        foreach($this->findAllDirectChildren($parentId) as $role) {
            //visited a role, so add it to the list.
            $children[] = $role;
            //now recursively get each role's subordinates
            $children = array_merge($children, $this->findAllChildren($role->id));
        }
        return $children;
    }

    public function findAllDirectChildren(int $parentId) : array
    {
        $roles = [];
        foreach($this->store->getAll() as $role) {
            if ($role->parentId == $parentId) {
                $roles[] = $role;
            }
        }
        
        return $roles;
    }
}