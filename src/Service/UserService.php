<?php
namespace Deputy\CodingChallenge\Service;

use Deputy\CodingChallenge\Model\User;
use Deputy\CodingChallenge\Store\UserStore;
use Deputy\CodingChallenge\Store\FileUserStore;

class UserService
{
    protected UserStore $store;
    protected RoleService $roleService;

    public function __construct(UserStore $userStore = null, RoleService $roleService = null)
    {
        $this->store = $userStore ?? new FileUserStore();
        $this->roleService = $roleService ?? new RoleService();
    }

    public function findAll() : array
    {
        return $this->store->getAll();
    }

    public function find(int $userId) : User
    {
        return $this->store->get($userId);
    }

    public function findAllWithRoleId(int $roleId) : array
    {
        $users = [];
        foreach($this->store->getAll() as $user) {
            if ($user->roleId == $roleId) {
                $users[] = $user;
            }
        }
        
        return $users;
    }

    public function findSubordinates(int $userId) : array
    {
        $user = $this->store->get($userId);

        $childRoles = $this->roleService->findAllChildren($user->roleId);
        
        $subordinateUsers = [];
        foreach($childRoles as $role) {
            $subordinateUsers = array_merge($subordinateUsers, $this->findAllWithRoleId($role->id));
        }

        return $subordinateUsers;
    }
}