<?php
namespace Deputy\CodingChallenge\Store;

use Deputy\CodingChallenge\Model\Role;

class FileRoleStore implements RoleStore
{
    protected string $path;
    protected array $roles = [];
    protected bool $loaded = false;

    public function __construct($path = 'data/roles.json')
    {
        $this->path = $path;
    }

    public function getAll() : array
    {
        $this->loadRoles();
        return $this->roles;
    }

    public function get(int $roleId) : Role
    {
        $this->loadRoles();

        if (!isset($this->roles[$roleId]))
            throw new \OutOfBoundsException("No Role with Id '{$roleId}'");

        return $this->roles[$roleId];
    }

    protected function loadRoles() : void
    {
        if ($this->loaded)
            return;

        //ideally we'd do more error checking around this
        $data = file_get_contents($this->path);
        foreach(json_decode($data, true) as $roleData) {
            $role = new Role();
            $role->hydrate($roleData);
            $this->roles[$role->id] = $role;
        }
        $this->loaded = true;
    }
}