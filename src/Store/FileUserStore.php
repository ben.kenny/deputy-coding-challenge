<?php
namespace Deputy\CodingChallenge\Store;

use Deputy\CodingChallenge\Model\User;

class FileUserStore implements UserStore
{
    protected string $path;
    protected array $users = [];
    protected bool $loaded = false;

    public function __construct($path = 'data/users.json')
    {
        $this->path = $path;
    }

    public function getAll() : array
    {
        $this->loadUsers();
        return $this->users;
    }

    public function get(int $userId) : User
    {
        $this->loadUsers();

        if (!isset($this->users[$userId]))
            throw new \OutOfBoundsException("No User with Id '{$userId}'");

        return $this->users[$userId];
    }

    protected function loadUsers() : void
    {
        if ($this->loaded)
            return;

        //ideally we'd do more error checking around this
        $data = file_get_contents($this->path);
        foreach(json_decode($data, true) as $userData) {
            $user = new User();
            $user->hydrate($userData);
            $this->users[$user->id] = $user;
        }
        $this->loaded = true;
    }
}