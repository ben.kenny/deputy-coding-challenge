<?php
namespace Deputy\CodingChallenge\Store;

use Deputy\CodingChallenge\Model\Role;

interface RoleStore
{
    public function getAll() : array;
    public function get(int $roleId) : Role;
}