<?php
namespace Deputy\CodingChallenge\Store;

use Deputy\CodingChallenge\Model\User;

interface UserStore
{
    public function getAll() : array;
    public function get(int $userId) : User;
}