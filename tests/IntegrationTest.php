<?php

use Deputy\CodingChallenge\Tests\TestCase;
use Deputy\CodingChallenge\Service\UserService;

/**
 * "Integration" tests for the services and roles together, using real data.
 */
final class IntegrationTest extends TestCase
{
    public function testGetSubordinates(): void
    {
        $userService = new UserService();

        //these test cases come from the PDF examples.
        $users = $userService->findSubordinates(3);
        $this->assertCount(2, $users);
        $this->assertContains($userService->find(2), $users); //{"Id": 2,"Name": "Emily Employee","Role": 4}
        $this->assertContains($userService->find(5), $users); //{"Id": 5, "Name": "Steve Trainer","Role": 5}

        $users = $userService->findSubordinates(1);
        $this->assertCount(4, $users);
        $this->assertContains($userService->find(2), $users); //{"Id": 2,"Name": "Emily Employee","Role": 4}
        $this->assertContains($userService->find(3), $users); //{"Id": 3,"Name": "Sam Supervisor","Role": 3}
        $this->assertContains($userService->find(4), $users); //{"Id": 4,"Name": "Mary Manager","Role": 2}
        $this->assertContains($userService->find(5), $users); //{"Id": 5, "Name": "Steve Trainer","Role": 5}
    }
}
