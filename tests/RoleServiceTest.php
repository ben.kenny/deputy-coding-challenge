<?php

use Deputy\CodingChallenge\Tests\TestCase;
use Deputy\CodingChallenge\Store\RoleStore;
use Deputy\CodingChallenge\Service\RoleService;

final class RoleServiceTest extends TestCase
{
    public function testReturnsAllRolesInTheStore(): void
    {
        $fixture = [
            $this->fakeRole(),
            $this->fakeRole(),
            $this->fakeRole(),
            $this->fakeRole()
        ];

        $storeMock = Mockery::mock(RoleStore::class);
        $storeMock->shouldReceive('getAll')
            ->andReturn($fixture);

        $roles = (new RoleService($storeMock))->findAll();

        $this->assertCount(4, $roles);
        $this->assertEquals($fixture, $roles);
    }

    public function testFindsARoleById(): void
    {
        $roleId = self::$faker->randomNumber();
        $expectedRole = $this->fakeRole(['id' => $roleId]);

        $storeMock = Mockery::mock(RoleStore::class);
        $storeMock->shouldReceive('get')
            ->andReturn($expectedRole);

        $actualRole = (new RoleService($storeMock))->find($roleId);

        $this->assertEquals($expectedRole, $actualRole);
    }

    public function testItCanGetRolesByParentId(): void
    {
        /**
         * setup the roles thus:
         * boss ---> employee --> technician second class --> series 4000 mechanoid
         *      |--> trainer 
         */
        $bossRole = $this->fakeRole(['name' => 'manager']);
        $employeeRole = $this->fakeRole(['name' => 'employee', 'parent' => $bossRole->id]);
        $trainerRole = $this->fakeRole(['name' => 'trainer', 'parent' => $bossRole->id]);
        $cleanerOfTheChickenSoupMachineRole = $this->fakeRole(['name' => 'technician second class', 'parent' => $employeeRole->id]);
        $krytenRole = $this->fakeRole(['name' => 'Series 4000 Mechanoid', 'parent' => $cleanerOfTheChickenSoupMachineRole->id]);
        
        $storeMock = Mockery::mock(RoleStore::class);
        $storeMock->shouldReceive('getAll')
            ->andReturn([
                $bossRole,
                $employeeRole,
                $trainerRole,
                $cleanerOfTheChickenSoupMachineRole,
                $krytenRole
            ]);

        $roles = (new RoleService($storeMock))->findAllDirectChildren($bossRole->id);
        
        //we should only have role that are direct decendants of the boss role
        $this->assertCount(2, $roles);
        $this->assertContains($employeeRole, $roles);
        $this->assertContains($trainerRole, $roles);

        $this->assertNotContains($bossRole, $roles);
        $this->assertNotContains($krytenRole, $roles);
        $this->assertNotContains($cleanerOfTheChickenSoupMachineRole, $roles);
    }

    public function testItCanRecursivelyGetChildRoles(): void
    {
        /**
         * setup the roles thus:
         * boss ---> employee --> technician second class --> series 4000 mechanoid
         *      |--> trainer 
         */
        $bossRole = $this->fakeRole(['name' => 'manager']);
        $employeeRole = $this->fakeRole(['name' => 'employee', 'parent' => $bossRole->id]);
        $trainerRole = $this->fakeRole(['name' => 'trainer', 'parent' => $bossRole->id]);
        $cleanerOfTheChickenSoupMachineRole = $this->fakeRole(['name' => 'technician second class', 'parent' => $employeeRole->id]);
        $krytenRole = $this->fakeRole(['name' => 'Series 4000 Mechanoid', 'parent' => $cleanerOfTheChickenSoupMachineRole->id]);
        
        $storeMock = Mockery::mock(RoleStore::class);
        $storeMock->shouldReceive('getAll')
            ->andReturn([
                $bossRole,
                $employeeRole,
                $trainerRole,
                $cleanerOfTheChickenSoupMachineRole,
                $krytenRole
            ]);

        $roles = (new RoleService($storeMock))->findAllChildren($employeeRole->id);
        
        //we should only have roles under the employee role
        $this->assertCount(2, $roles);
        $this->assertContains($cleanerOfTheChickenSoupMachineRole, $roles);
        $this->assertContains($krytenRole, $roles);        
        
        $this->assertNotContains($bossRole, $roles);
        $this->assertNotContains($employeeRole, $roles);
        $this->assertNotContains($trainerRole, $roles);

    }

    public function tearDown(): void
    {
        \Mockery::close();
    }
}
