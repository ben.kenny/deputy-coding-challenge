<?php
use Deputy\CodingChallenge\Tests\TestCase;
use Deputy\CodingChallenge\Model\Role;

final class RoleTest extends TestCase
{
    public function testCanHydrateARole(): void
    {

        $id = self::$faker->randomNumber();
        $name = self::$faker->name();
        $parent = self::$faker->randomNumber();
        
        $user = new Role();
        $user->hydrate([
            'id' => $id,
            'name' => $name,
            'parent' => $parent
        ]);

        $this->assertEquals($id, $user->id);
        $this->assertEquals($name, $user->name);
        $this->assertEquals($parent, $user->parentId);
    }
}
