<?php
namespace Deputy\CodingChallenge\Tests;

use Deputy\CodingChallenge\Model\Role;
use Deputy\CodingChallenge\Model\User;

class TestCase extends \PHPUnit\Framework\TestCase
{
    protected static $faker;

    public function __construct()
    {
        parent::__construct();
        self::$faker = \Faker\Factory::create();
    }

    //It would make more sense to register our own provider for Faker
    public function fakeRole($overrides = []) : Role
    {
        $role = new Role();
        $role->hydrate(
            array_merge([
                'id' => self::$faker->randomNumber(),
                'name' => self::$faker->name(),
                'parent' => self::$faker->randomNumber(),
            ], $overrides)
        );
        
        return $role;
    }

    public function fakeUser($overrides = []) : User
    {
        $role = new User();
        $role->hydrate(
            array_merge([
                'id' => self::$faker->randomNumber(),
                'name' => self::$faker->name(),
                'role' => self::$faker->randomNumber(),
            ], $overrides)
        );
        
        return $role;
    }
}