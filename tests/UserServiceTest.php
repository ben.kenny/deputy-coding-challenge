<?php

use Deputy\CodingChallenge\Tests\TestCase;
use Deputy\CodingChallenge\Store\UserStore;
use Deputy\CodingChallenge\Service\UserService;
use Deputy\CodingChallenge\Service\RoleService;

final class UserServiceTest extends TestCase
{
    public function testReturnsAllUsersInTheStore(): void
    {
        $fixture = [
            $this->fakeUser(),
            $this->fakeUser(),
            $this->fakeUser(),
            $this->fakeUser(),
            $this->fakeUser(),
            $this->fakeUser()
        ];

        $storeMock = Mockery::mock(UserStore::class);
        $storeMock->shouldReceive('getAll')
            ->andReturn($fixture);

        $users = (new UserService($storeMock, null))->findAll();

        $this->assertCount(6, $users);
        $this->assertEquals($fixture, $users);
    }

    public function testFindsAUserById(): void
    {
        $userId = self::$faker->randomNumber();
        $expectedUser = $this->fakeUser(['id' => $userId]);

        $storeMock = Mockery::mock(UserStore::class);
        $storeMock->shouldReceive('get')
            ->andReturn($expectedUser);

        $actualUser = (new UserService($storeMock, null))->find($userId);

        $this->assertEquals($expectedUser, $actualUser);
    }

    public function testFindAllUsersWithAGivenRole(): void
    {
        $expectedRole = self::$faker->randomNumber();        
        $expectedUser1 = $this->fakeUser(['role' => $expectedRole]);
        $expectedUser2 = $this->fakeUser(['role' => $expectedRole]);

        $fixture = [
            $this->fakeUser(),
            $this->fakeUser(),
            $expectedUser2,
            $this->fakeUser(),
            $expectedUser1,
            $this->fakeUser()
        ];

        $storeMock = Mockery::mock(UserStore::class);
        $storeMock->shouldReceive('getAll')
            ->andReturn($fixture);

        $users = (new UserService($storeMock, null))->findAllWithRoleId($expectedRole);

        $this->assertCount(2, $users);
        $this->assertContains($expectedUser1, $users);
        $this->assertContains($expectedUser2, $users);
    }

    public function testItCanFindSubordinateUsers(): void
    {
        /**
         * setup the roles thus:
         * boss ---> employee --> technician second class
         *      |--> trainer 
         */
        $bossRole = $this->fakeRole(['name' => 'manager']);
        $employeeRole = $this->fakeRole(['name' => 'employee', 'parent' => $bossRole->id]);
        $trainerRole = $this->fakeRole(['name' => 'trainer', 'parent' => $bossRole->id]);
        $cleanerOfTheChickenSoupMachineRole = $this->fakeRole(['name' => 'technician second class', 'parent' => $employeeRole->id]);


        $boss = $this->fakeUser(['role' => $bossRole->id]);
        $employee = $this->fakeUser(['role' => $employeeRole->id]);
        $trainer = $this->fakeUser(['role' => $trainerRole->id]);
        $arnoldRimmer = $this->fakeUser(['role' => $cleanerOfTheChickenSoupMachineRole->id]);

        $storeMock = Mockery::mock(UserStore::class);
        $storeMock->shouldReceive('get')
            ->andReturn($boss);
        $storeMock->shouldReceive('getAll')
            ->andReturn([
                $boss,
                $employee,
                $trainer,
                $arnoldRimmer
            ]);

        $roleServiceMock = Mockery::mock(RoleService::class);
        $roleServiceMock->shouldReceive('findAllChildren')
            ->andReturn([
                $employeeRole,
                $trainerRole,
                $cleanerOfTheChickenSoupMachineRole
            ]);


        $users = (new UserService($storeMock, $roleServiceMock))->findSubordinates($boss->id);


        $this->assertCount(3, $users);
        
        $this->assertContains($employee, $users);
        $this->assertContains($trainer, $users);
        $this->assertContains($arnoldRimmer, $users);
        
        $this->assertNotContains($boss, $users);
    }

    public function tearDown(): void
    {
        \Mockery::close();
    }
}
