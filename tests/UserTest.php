<?php
use Deputy\CodingChallenge\Tests\TestCase;
use Deputy\CodingChallenge\Model\User;

final class UserTest extends TestCase
{
    public function testCanHydrateAUser(): void
    {

        $id = self::$faker->randomNumber();
        $name = self::$faker->name();
        $role = self::$faker->randomNumber();
        
        $user = new User();
        $user->hydrate([
            'id' => $id,
            'name' => $name,
            'role' => $role
        ]);

        $this->assertEquals($id, $user->id);
        $this->assertEquals($name, $user->name);
        $this->assertEquals($role, $user->roleId);
    }
}
